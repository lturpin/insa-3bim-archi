#include <stdio.h>
#include <time.h>

int main()
{
  // declarations
  int n,i;
  long R_inter,R_final;
  
  // initializations
  printf("enter the value of n : ");
  scanf("%d",&n);
  R_final = 0;

  int t0 = clock();
  // computation of the sum of factorial
  R_inter=1;
  for (i=1 ; i<=n ; i++)
    {
      R_inter = R_inter * i;
      R_final = R_final + R_inter;
    }
  printf("%ld\n",R_final);

  printf("execution time : %lf seconds\n",(clock()-t0)/(double)CLOCKS_PER_SEC);
}

  
