#include <stdio.h>

int main()
{
  float  pi_1 = 3.1415926535;
  double pi_2 = 3.1415926535;
  float  pi_3 = 3.14159265;
  float  pi_4 = 3.141592741;

  printf("pi_1 = 3.1415926535\npi_2 = 3.1415926535\npi_3 = 3.14159265\npi_4 = 3.141592741\n\n");

  if (pi_1 == pi_2) printf("pi_1 = pi_2\n"); else printf("pi_1 != pi_2\n");
  if (pi_1 == pi_3) printf("pi_1 = pi_3\n"); else printf("pi_1 != pi_3\n");
  if (pi_1 == pi_4) printf("pi_1 = pi_4\n"); else printf("pi_1 != pi_4\n");
}
