#include <stdio.h>
#include <time.h>

int main()
{
  // declarations
  long n; // <- watch out ! Here is the difference
  int i,j;

  // initializations
  printf("enter the value of n : ");
  scanf("%ld",&n);

  int t0 = clock();
  for (i=1 ; i<=n ; i=i+1)
    {
      j=i;
      if (j%100000 == 0) // <- not very important, it prevents your terminal to be flooded by numbers. It will also speed up the execution, and you should know why by now ;)
	      printf("j = %d\n",j);
    }
  printf("execution time : %lf seconds\n",(clock()-t0)/(double)CLOCKS_PER_SEC);
}
