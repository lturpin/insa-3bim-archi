#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <assert.h>

char * tt(int64_t bb)
{
  const char *uu[]={"B","kB","MB","GB","TB","PB","EB","ZB","YB"};
  int u=0;
  char* result = malloc(7);
  assert(result != NULL);
  float nb=bb;
  while(nb>1000)
    {
      nb/=1024;
      u++;
    }
  snprintf(result,7,"%.3g%s",nb,uu[u]);
  return result;
}

void bmk(int64_t N, int64_t R,int shuffle)
{

  // allocation of a big list in memory heap
  int64_t *tab = (int64_t *) malloc(N * sizeof(int64_t));
  assert(tab != NULL);
  int64_t i;
  for (i=0;i<N;i++)
    {
      tab[i]=i;
    }

  // random shuffle of the list
  srandom(clock());
  i=N;
  while(i>1)
    {
      i--;
      int64_t j=random()%i;
      int64_t tmp=tab[i];
      tab[i]=tab[j];
      tab[j]=tmp;
    }
  
  // run through the list
  int64_t t0=clock();

  for (int64_t r=0;r<R;r++)
    {
      int64_t p=0;
      while (tab[p] != 0)
        {
          p=tab[p];
        }
    }
  int64_t t1=clock();
  double t=1000000000*(t1-t0)/CLOCKS_PER_SEC/R;

  // display results
  printf("%15ld            %6.5g",N*sizeof(int64_t),t/N);
  printf("       %15ld",N);
  printf("            %5ld",R);
  printf("          %s",tt(N*sizeof(int64_t)));
  printf("\n");
}

int main()
{
  int64_t R=10000;
  int64_t N=32;
  printf("           size         access time(ns)      Nb of elements      nb Repeats       size(human readable)\n");
  while(1)
    {
      int64_t t0=clock();
      bmk(N,R,1);
      int64_t t1=clock();
      N=N*1.2;
      if (t1-t0 > CLOCKS_PER_SEC/2)
        {
          if(R>1) R=R/2;
        }
      
    }
  return 0;
}
