#include <stdio.h>
#include <time.h>

int main()
{
  // declaration
  int n,i;
  FILE * fic = fopen("./fic.txt","w");

  // initializations
  printf("enter the value of n : ");
  scanf("%d",&n);

  int t0 = clock();
  for (i=1 ; i<=n ; i=i+1)
    {
      fprintf(fic,"%d\n",i); // write the value of i in the file fic.txt
    }
  printf("i = %d\n",i);
  printf("execution time : %lf seconds\n",(clock()-t0)/(double)CLOCKS_PER_SEC);
  fclose(fic);
}
