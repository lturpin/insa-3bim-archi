#include <stdio.h>
#include <time.h>

int main()
{
  // declarations
  float n,s;

  int t0 = clock();
  
  for (n=1;n<=100000;n++)
    {
      s = s + (1.0/(n*n));
    }
  printf("Sum from n=1 to n=100.000 of 1/n2 = %f\n",s);

  printf("execution time : %lf seconds\n",(clock()-t0)/(double)CLOCKS_PER_SEC);
}

  
