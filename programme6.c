#include <stdio.h>
#include <time.h>

int main()
{
  // declarations
  int n,i,j;
  long R_inter,R_final;

  // initializations
  printf("enter the value of n : ");
  i=scanf("%d",&n);
  R_final = 0;

  int t0 = clock();
  // computation of the sum
  for (i=1 ; i<=n ; i++)
    {
      // computation of factorial
      R_inter = 1;
      for (j=1 ; j<=i ; j++)
        {
          R_inter = R_inter * j;
        }
      R_final = R_final + R_inter;
    }
  printf("%ld\n",R_final);

  printf("execution time : %lf seconds\n",(clock()-t0)/(double)CLOCKS_PER_SEC);
}

  
