# Computer Architecture tutorial #2

During the first computer architecture lecture, we saw (tried to see?), from an essentially theoretical point of view, how is structured a _von Neumann machine_ as well as the constraints imposed by the binary coding of the information.


During this second lecture, we will see (try to see?) the practical implications of the architecture. For that, you will manipulate codes in the "C" language, try to understand what they do or should do, test them on your machine and check whether they meet your expectations -- or not! In this second case, we will try to understand together where the difference comes from... We will use the C language here (although you do not know it) because it is a language that allows to "get closer" to the machine, so to see the influence of its architecture. Note, however, that most of the points we will discuss in this tutorial are language independent (they are the manifestation of the underlying architecture). Also note that the codes we will handle are simple enough - except the last one - so that you can understand them without knowing the language itself...

_Be careful, there are 11 exercises. Most should not take more than 5 to 10 minutes. Try to keep this pace so that we can devote more time to the last exercise (~30 minutes!)._

## Exercise 1:

* Download the file [programme1.c](programme1.c) and open it with your favorite text editor. What do you think this program does?
* C being a compiled language (make sure you understand what it means, if not ask!), you will have to start by turning this code into machine language. This is done by the following command:

```bash
gcc -O0 programme1.c -o programme1.out
```
The "-o" compiler option tells the compiler what name it should give to the executable program. The option "-O0" (big o, zero) tells the compiler that it should not optimize the program in any way (you can try -O1 or -O2 if you're curious).

* you can then start your program with the following command:
```bash
./programme1.out
```

* What does this program do? Try several input values. How does the execution time vary according to the number of entries? Test this program with n = 100,000 and note the execution time.
* Try to give the program a negative input value; What is going on?
* Try to give the program an alphabetical input value (for instance "aa"); What is going on?
* Try to give the program a very large input value (try 1, 2, 3 and 4 billion); What is going on? (look at both the result and the execution time).
* Interpret the previous results in terms of computer architecture.

## Exercise 2:

* Download the file [programme2.c](programme2.c) and open it with your favorite text editor. What is the difference with the previous program?
* Compile programme2.c,

```bash
gcc -O0 programme2.c -o programme2.out
```

* Test it with n = 100,000. What is the difference in execution time compared to program1?
* Interpret this result in terms of computer architecture.

## Exercise 3:

* Download the file [programme3.c](programme3.c) and open it with your favorite text editor. What is the difference with programme1.c?
* Compile programme3.c

```bash
gcc -O0 programme3.c -o programme3.out
```

* Test it with n = 100,000. What is the difference in execution time compared to program1 and program2?
* Interpret this result in terms of computer architecture.

## Exercise 4:

* Download the file [programme4.c](programme4.c) and open it with your favorite text editor. What is the difference with programme1.c?
* Compile programme4.c

```bash
gcc -O0 programme4.c -o programme4.out
```

* Test program4 with very large values ​​(typically 1, 2, 3 and 4 billion). What is going on?
* Interpret this result in terms of computer architecture.

## Exercise 5:

* Download the file [programme5.c](programme5.c) and open it with your favorite text editor. What is the difference with programme4.c?
* Compile programme5.c

```bash
gcc -O0 programme5.c -o programme5.out
```

* Test program5 with very large values ​​(typically 1, 2, 3 and 4 billion). What is going on?
* Interpret this result in terms of computer architecture.

## Exercise 6:

* Download the file [programme6.c](programme6.c), open it, look at it, compile it, execute it...
* This program computes the sum of factorials from 1 to n...
* Test it with different values ​​of n; What is going on?
  * Regarding the result? (test values ​​between 1 and 30)
  * Regarding the computation time? (test values ​​between 10,000 and 100,000)
  * Compile your program again but with the -O1 option; test the same values ​​of n; What is going on ?
* Interpret these results in terms of computer architecture and compilation.

## Exercise 7:

* Download the file [programme7.c](programme7.c), open it, look at it, compile it, execute it...
* This program computes the sum of the factorials from 1 to n; what is the difference between programme7 and programme6?
* Test it with the same values ​​of n as before; What is going on?
  * Regarding the result?
  * Regarding the computation time?
* Interpret these results in terms of computer architecture and compilation.

## Exercise 8:

* Download the file [programme8.c](programme8.c), open it, look at it...
* What do you think this program should display?
* Compile it, run it... behaving as expected?
* Interpret the result in terms of computer architecture and data encoding.

## Exercise 9:

* Download the file [programme9.c](programme9.c), open it, look at it...
* What should it display?
* Compile it, run it... What does it display?
* Using a text editor, try to change the equation in the test, for example by replacing
```
if (((pi3-pi2)-pi1) == 0)
```
by
```
if (((pi3-pi1)-pi2) == 0)
```
or by
```
if ((pi3-(pi2+pi1)) == 0)
```

* What does this change on the result of the test (note that the printed result becomes inconsistent but this is not what we want you to consider...).
* Interpret the result in terms of computer architecture and data encoding.

## Exercise 10:

* Download the file [programme10.1.c](programme10.1.c); open it, look at it...
* Download the file [programme10.2.c](programme10.2.c), open it, look at it...
* Both programs compute exactly the same thing: the sum of 1/n<sup>2</sup> for n ranging from 1 to 100,000 (program 10.1) and for n ranging from 100,000 to 1 (program 10.2). They should therefore give the same result...
* Compile and run these two programs. What are their respective results?
* Interpret these results in terms of computer architecture and floating-point encoding (knowing that the exact value is: 1.64492407)

## Exercise 11 :

_A big thank you to Guillaume Salagnac to whom I owe the idea and the code of this exercise. If you want to go further, you can consult Guillaume's GitHub page about the measure of "memory mountain"._

* Download the file [programme11.c](programme11.c), open it, look at it... Give up, it's incomprehensible!
* Read what follows carefully and make sure that you understand (if not ... ask!). In practice, this program does nothing more than create an array in memory, it then initializes it and browses it in a random order. While browsing the array, it measures the time necessary to read it, which makes it possible to estimate the access time to the memory (access time is approximately equal to the total time divided by the size of the array). It then increases a little bit the size of the array (x1.2) and starts the process again again (it also automatically manages the number of repetitions of the test in order to increase the accuracy). To conclude, program 11 allow us to observe how the memory access time varies according to the size of the accessed data.
* Compile this program and run it... (it does not stop by itself - if you want to stop it, you have to kill it with a "Ctrl + C" - a "control-c").
* While program is running, you can run parallel (in another terminal) the Linux "free" command ("man free" if you want to know more about this command...) with the following options:

```bash
free -m -s 1
```

* How does the memory access time change?
* Copy and paste the result of program11.c (displayed in the terminal) into a file and look at the first two columns (i.e. column 2 as a function of column 1, in log scales) in your favorite stat tool (let's say... hmm, R?)... what does it look like? This curve is sometimes called the "memory mountain"; as an example, here is what gives the execution of this program on macbook ([here](memory_mountain.png))
* The "theory" tells you that the memory of your machine is a RAM ("Random Access Memory"). What does it mean? Is it coherent with your previous conclusions?
* Interpret these results in terms of computer architecture (if you have no idea... ask!).
* Compare your results/conclusions with the output of the following Linux command:

```bash
lscpu
```

That's all folks ...