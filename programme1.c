#include <stdio.h>
#include <time.h>

int main()
{
  // declarations
  int n,i,j;
  
  // initializations
  printf("enter the value of n : ");
  scanf("%d",&n);

  int t0 = clock();
  for (i=1 ; i<=n ; i=i+1)
    {
      j=i;
    }
  printf("j = %d\n",j);
  printf("execution time : %lf seconds\n",(clock()-t0)/(double)CLOCKS_PER_SEC);
}
